﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/**
 * From now on, we will create the puzzle solutions in the PuzzleSolvers namespace.
 */
namespace challenge_inscription
{
    [Obsolete("Deprecated use of Anto's Code. Use PuzzleSolvers instead.", true)]
    public class CodeAnto
    {
        public static String lettre0(string[][] array)
        {
            String sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "." && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "." && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "C";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X" && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X" && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "X" && array[0][1] == "." && array[0][2] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X" && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X" && array[4][0] == "." && array[4][1] == "X" && array[4][2] == ".")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "." && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }

        /*retourne la lettre a 90 degree dans larray de [3][5] si il trouve rien il retourne A*/
        public static String lettre90(string[][] array)
        {
            String sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "." && array[2][3] == "." && array[2][4] == "X")
            {
                sortie = "C";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X" && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "." && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "." && array[2][0] == "." && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[1][3] == "." && array[1][4] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X" && array[2][3] == "." && array[2][4] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }


        /*retourne la lettre a 180 degree dans larray de [5][3] si il trouve rien il retourne A*/
        public static String lettre180(string[][] array)
        {
            String sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "." && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "." && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "C";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X" && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X" && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "X" && array[0][1] == "." && array[0][2] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X" && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X" && array[4][0] == "." && array[4][1] == "X" && array[4][2] == ".")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" 
                && array[1][0] == "." && array[1][1] == "." && array[1][2] == "X" 
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X"
                && array[3][0] == "." && array[3][1] == "." && array[3][2] == "X" 
                && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }


        /*retourne la lettre a 270 degree dans larray de [3][5] si il trouve rien il retourne A*/
        public static String lettre270(string[][] array)
        {
            String sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "." && array[2][3] == "." && array[2][4] == "X")
            {
                sortie = "C";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X" && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "." && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "." && array[2][0] == "." && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X" && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[1][3] == "." && array[1][4] == "X" && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X" && array[2][3] == "." && array[2][4] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }

        public static String lettreV(string[][] array)
        {
            String sortie = "A";
            sortie = lettre0(array);
            if (sortie == "A")
            {
                sortie = lettre180(array);
            }
            return sortie;
        }


        public static String lettreH(string[][] array)
        {
            String sortie = "A";
            sortie = lettre90(array);
            if (sortie == "A")
            {
                sortie = lettre270(array);
            }
            return sortie;
        }

        /* retourne la matrice [5][3] si verticale et [3][5] si horzontale
         x et y represente lindice du coin haut gauche dans la matrice totale*/
        public static string[][] SortirMatrice(string[][] array, int x, int y, bool horizontale)
        {
            string[][] sortie;
            if (horizontale)
            {
                sortie = new string[3][];
                try
                {
                    for (int j = y; j < y + 3; j++)
                    {
                        sortie[j - y] = new string[5];
                        for (int i = x; i < x + 5; i++)
                        {
                            sortie[j - y][i - x] = array[j][i];

                        }
                    }
                } catch (IndexOutOfRangeException e)
                {
                    for (int i = 0; i<sortie.Count(); i++)
                    {
                        sortie[i] = new string[5];
                        for (int j = 0; j < sortie[0].Count(); j++)
                        {
                            sortie[i][j] = "X";
                        }
                    }
                }
            }
            else
            {
                sortie = new string[5][];
                try
                {
                    for (int j = y; j < y + 5; j++)
                    {
                        sortie[j - y] = new string[3];
                        for (int i = x; i < x + 3; i++)
                        {
                            sortie[j - y][i - x] = array[j][i];

                        }
                    }
                } catch (IndexOutOfRangeException e)
                {
                    for (int i = 0; i < sortie.Count(); i++)
                    {
                        sortie[i] = new string[3];
                        for (int j = 0; j < sortie[0].Count(); j++)
                        {
                            sortie[i][j] = "X";
                        }
                    }
                }
            }
            return sortie;
        }


        public static string[] SolveAll(string[][][] puzzles)
        {

            string[] reponse = new string[8];
            string[][] puzzle1 = puzzles[0];
            string[][] puzzle2 = puzzles[1];
            string[][] puzzle3 = puzzles[2];
            string[][] puzzle4 = puzzles[3];
            string[][] puzzle5 = puzzles[4];
            string[][] puzzle6 = puzzles[5];
            string[][] puzzle7 = puzzles[6];
            string[][] puzzle8 = puzzles[7];

            /*puzzle 1*/
            reponse[0] = lettre0(puzzle1);


            /*puzzle 2*/
            if (puzzle2.Count() == 5)
            {
                reponse[1] = lettre0(puzzle2);
                if (reponse[1] == "A")
                    reponse[1] = lettre180(puzzle2);
            }
            else
            {
                reponse[1] = lettre90(puzzle2);
                if (reponse[1] == "A")
                    reponse[1] = lettre270(puzzle2);
            }

            /*puzzle 3*/
            reponse[2] = lettre0(SortirMatrice(puzzle3, 1, 0, false)) + lettre0(SortirMatrice(puzzle3, 5, 0, false)) + lettre0(SortirMatrice(puzzle3, 9, 0, false)) + lettre0(SortirMatrice(puzzle3, 13, 0, false)) + lettre0(SortirMatrice(puzzle3, 17, 0, false));

            /*puzzle 4*/
            String temp = "A";
            int x = 1;
            reponse[3] = "";
            
            while (reponse[3].Length < 5)
            {
                temp = lettreV(SortirMatrice(puzzle4, x, 0, false));
                if (temp == "A")
                {
                    x = x + 6;
                    temp = lettreH(SortirMatrice(puzzle4, x, 0, true));
                    if (temp == "A")
                    {
                        temp = lettreH(SortirMatrice(puzzle4, x, 1, true));
                        if (temp == "A")
                        {
                            temp = lettreH(SortirMatrice(puzzle4, x, 2, true));
                        }
                    }
                }
                else
                {
                    x = x + 4;
                }
                reponse[3] += temp;
            }

            /*puzzle 5*/
            reponse[4] = lettre0(SortirMatrice(puzzle5, 1, 1, false)) + lettre0(SortirMatrice(puzzle5, 5, 1, false)) + lettre0(SortirMatrice(puzzle5, 9, 1, false)) + lettre0(SortirMatrice(puzzle5, 13, 1, false)) + lettre0(SortirMatrice(puzzle5, 17, 1, false));
            reponse[4] += lettre0(SortirMatrice(puzzle5, 1, 7, false)) + lettre0(SortirMatrice(puzzle5, 5, 7, false)) + lettre0(SortirMatrice(puzzle5, 9, 7, false)) + lettre0(SortirMatrice(puzzle5, 13, 7, false)) + lettre0(SortirMatrice(puzzle5, 17, 7, false));
            reponse[4] += lettre0(SortirMatrice(puzzle5, 1, 13, false)) + lettre0(SortirMatrice(puzzle5, 5, 13, false)) + lettre0(SortirMatrice(puzzle5, 9, 13, false)) + lettre0(SortirMatrice(puzzle5, 13, 13, false)) + lettre0(SortirMatrice(puzzle5, 17, 13, false));
            reponse[4] += lettre0(SortirMatrice(puzzle5, 1, 19, false)) + lettre0(SortirMatrice(puzzle5, 5, 19, false)) + lettre0(SortirMatrice(puzzle5, 9, 19, false)) + lettre0(SortirMatrice(puzzle5, 13, 19, false)) + lettre0(SortirMatrice(puzzle5, 17, 19, false));
            reponse[4] += lettre0(SortirMatrice(puzzle5, 1, 25, false)) + lettre0(SortirMatrice(puzzle5, 5, 25, false)) + lettre0(SortirMatrice(puzzle5, 9, 25, false)) + lettre0(SortirMatrice(puzzle5, 13, 25, false)) + lettre0(SortirMatrice(puzzle5, 17, 0, false));


            /*puzzle 6*/
            temp = "A";
            x = 1;
            reponse[5] = "";
            for (int i = 0; i < 5; i++)
            {
                while (reponse[5].Count() < 5 * i)
                {
                    temp = lettreV(SortirMatrice(puzzle6, x, (i * 6 + 1), false));
                    if (temp == "A")
                    {
                        x = x + 6;
                        temp = lettreH(SortirMatrice(puzzle6, x, (i * 6 + 1), true));
                        if (temp == "A")
                        {
                            temp = lettreH(SortirMatrice(puzzle6, x, (i * 6 + 2), true));
                            if (temp == "A")
                            {
                                temp = lettreH(SortirMatrice(puzzle6, x, (i * 6 + 3), true));
                            }
                        }
                    }
                    else
                    {
                        x = x + 4;
                    }
                    reponse[5] += temp;
                }
            }
            return reponse;
            /*puzzle 7*/



            /*puzzle 8*/




            // Console.WriteLine(puzzles[0][0][0]);
        }
    }
}
