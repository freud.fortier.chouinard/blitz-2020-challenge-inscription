﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle6 : AbstractPuzzle
    {
        public Puzzle6(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            String CurrentLetter = "A";
            string Solution = "";

            for (int i = 0; i < 5; i++)
            {
                for (int y = 1; y < 4; y++)
                {
                    for (int x = 1; x < Grid[0].Count();)
                    {
                        CurrentLetter = lettreV(ExtractSubMatrix(x, i * 6 + y, false));
                        if (!IsValidLetter(CurrentLetter))
                        {

                            CurrentLetter = lettreH(ExtractSubMatrix(x, i * 6 + y, true));
                            if (IsValidLetter(CurrentLetter))
                                x += 6;
                            else
                                x++;
                        }
                        else
                        {
                            x += 4;
                        }
                        if (IsValidLetter(CurrentLetter))
                            Solution += CurrentLetter;
                    }
                }
            }
            return Solution;
        }

    }
}
