﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle7 : AbstractPuzzle
    {
        public Puzzle7(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            String CurrentLetter = "A";
            string Solution = "";

            for (int y = 0; y < Grid.Count(); y++)
            {
                for (int x = 0; x < Grid[0].Count();)
                {
                    CurrentLetter = GetPaddedVerticalLetter(ExtractSubMatrix(x, y, false, true));
                    if (!IsValidLetter(CurrentLetter))
                    {

                        CurrentLetter = GetPaddedHorizontalLetter(ExtractSubMatrix(x, y, true, true));
                        if (IsValidLetter(CurrentLetter))
                            x += 6;
                        else
                            x++;
                    }
                    else
                    {
                        x += 4;
                    }
                    if (IsValidLetter(CurrentLetter))
                        Solution += CurrentLetter;
                }
            }
            return Solution;
        }
    }
}
