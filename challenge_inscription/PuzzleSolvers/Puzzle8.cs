﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle8 : AbstractPuzzle
    {
        public Puzzle8(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            String CurrentLetter = "A";
            string Solution = "";
            string SolutionTemp = "";
            List<Point> lettres = new List<Point>();
           
                for (int y = 0; y < Grid.Count(); y++)
                {
                    for (int x = 0; x < Grid[0].Count();)
                    {
                        CurrentLetter = lettreV(ExtractSubMatrix(x, y, false));
                        if (!IsValidLetter(CurrentLetter))
                        {

                            CurrentLetter = lettreH(ExtractSubMatrix(x, y, true));
                            if (IsValidLetter(CurrentLetter))
                            {
                                for (int i = 0; i < 5; i++)
                                {
                                    for (int j = 0; j < 3; j++)
                                    {
                                        lettres.Add(new Point(x + i, y + j));
                                    }
                                }
                                x += 5;
                            }
                            else
                                x++;
                        }
                        else
                        {
                            for (int i = 0; i < 5; i++)
                            {
                                for (int j = 0; j < 3; j++)
                                {
                                    lettres.Add(new Point(x + j, y + i));
                                }
                            }
                            x += 3;
                        }
                        if (IsValidLetter(CurrentLetter))
                            SolutionTemp += CurrentLetter;
                    }
                }


                List<int> toRemove = new List<int>();
                for (int i = 0; i < lettres.Count(); i++)
                {
                    Point element = lettres.ElementAt(i);
                    int x = element.X;
                    int y = element.Y;
                    for (int j = -1; j <= 1; j++)
                    {
                        for (int k = -1; k <= 1; k++)
                        {
                            try { 
                            if (Grid[y + j][x + k] == "X" && !lettres.Contains(new Point(x + k, y + j)))
                            {
                                toRemove.Add(i / 15);

                            }}
                            catch (IndexOutOfRangeException)
                            {

                            }
                        }
                    }
                }


                for (int i = 0; i < SolutionTemp.Length; i++)
                {
                    if (!toRemove.Contains(i))
                    {
                        Solution += SolutionTemp.ElementAt(i);
                    }
                }

            
            
            return Solution;
        }
    }
}
