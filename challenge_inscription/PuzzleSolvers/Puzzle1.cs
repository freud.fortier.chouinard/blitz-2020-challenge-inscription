﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle1 : AbstractPuzzle
    {
        public Puzzle1(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            return lettre0(Grid);
        }
    }
}
