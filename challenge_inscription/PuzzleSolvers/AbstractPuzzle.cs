﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public abstract class AbstractPuzzle
    {

        protected string[][] Grid { get; set; }

        public AbstractPuzzle(string[][] grid) => Grid = grid;

        public static string lettre0(string[][] array)
        {
            string sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "."
                && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "."
                && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "."
                && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X"
                )
            {
                sortie = "C";
            }
            else if (
                array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X"
                && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X"
                && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X"
                && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "X" && array[0][1] == "." && array[0][2] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X"
                && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X"
                && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X"
                && array[4][0] == "." && array[4][1] == "X" && array[4][2] == ".")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "."
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X"
                && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "."
                && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }

        /*retourne la lettre a 90 degree dans larray de [3][5] si il trouve rien il retourne A*/
        public static string lettre90(string[][] array)
        {
            string sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X"
                && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "." && array[2][3] == "." && array[2][4] == "X")
            {
                sortie = "C";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X"
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "." && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "."
                && array[2][0] == "." && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[1][3] == "." && array[1][4] == "X"
                && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X" && array[2][3] == "." && array[2][4] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }


        /*retourne la lettre a 180 degree dans larray de [5][3] si il trouve rien il retourne A*/
        public static string lettre180(string[][] array)
        {
            string sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X"
                && array[1][0] == "." && array[1][1] == "." && array[1][2] == "X"
                && array[2][0] == "." && array[2][1] == "." && array[2][2] == "X"
                && array[3][0] == "." && array[3][1] == "." && array[3][2] == "X"
                && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "C";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X"
                && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X"
                && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X"
                && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "." && array[0][1] == "X" && array[0][2] == "."
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X"
                && array[2][0] == "X" && array[2][1] == "." && array[2][2] == "X"
                && array[3][0] == "X" && array[3][1] == "." && array[3][2] == "X"
                && array[4][0] == "X" && array[4][1] == "." && array[4][2] == "X")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X"
                && array[1][0] == "." && array[1][1] == "." && array[1][2] == "X"
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X"
                && array[3][0] == "." && array[3][1] == "." && array[3][2] == "X"
                && array[4][0] == "X" && array[4][1] == "X" && array[4][2] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }


        /*retourne la lettre a 270 degree dans larray de [3][5] si il trouve rien il retourne A*/
        public static string lettre270(string[][] array)
        {
            string sortie = "A";
            if (array[0][0] == "X" && array[0][1] == "." && array[0][2] == "." && array[0][3] == "." && array[0][4] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X"
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "C";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X"
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "O";
            }
            else if (array[0][0] == "X" && array[0][1] == "X" && array[0][2] == "X" && array[0][3] == "X" && array[0][4] == "."
                && array[1][0] == "." && array[1][1] == "." && array[1][2] == "." && array[1][3] == "." && array[1][4] == "X"
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == ".")
            {
                sortie = "V";
            }
            else if (array[0][0] == "X" && array[0][1] == "." && array[0][2] == "X" && array[0][3] == "." && array[0][4] == "X"
                && array[1][0] == "X" && array[1][1] == "." && array[1][2] == "X" && array[1][3] == "." && array[1][4] == "X"
                && array[2][0] == "X" && array[2][1] == "X" && array[2][2] == "X" && array[2][3] == "X" && array[2][4] == "X")
            {
                sortie = "E";
            }
            return sortie;
        }

        /**
         * Takes a 5 x 7 array and returns the corresponding letter. 
         * If it doesn't contain a letter padded with dots, returns "A".
         */
        public static string GetPaddedVerticalLetter(string[][] array)
        {
            if (HasPadding(array))
            {
                string[][] subArray = new string[5][];
                for (int i = 0; i < subArray.Count(); i++)
                {
                    subArray[i] = new string[3];
                    for (int j = 0; j < subArray[0].Count(); j++)
                    {
                        subArray[i][j] = array[i + 1][j + 1];
                    }
                }
                return lettreV(subArray);
            }
            else
            {
                return "A";
            }

        }

        public static string GetPaddedHorizontalLetter(string[][] array)
        {
            if (HasPadding(array))
            {
                string[][] subArray = new string[3][];
                for (int i = 0; i < subArray.Count(); i++)
                {
                    subArray[i] = new string[5];
                    for (int j = 0; j < subArray[0].Count(); j++)
                    {
                        subArray[i][j] = array[i + 1][j + 1];
                    }
                }
                return lettreH(subArray);
            }
            else
            {
                return "A";
            }

        }
        private static bool HasPadding(string[][] array)
        {
            // Verify the array has left and right padding
            for (int i = 0; i < array.Count(); i++)
            {
                if (array[i][0] != "." || array[i][array[0].Count() - 1] != ".")
                {
                    return false;
                }
            }

            // Verify the array has top and bottom padding
            for (int i = 0; i < array[0].Count(); i++)
            {
                if (array[0][i] != "." || array[array.Count() - 1][i] != ".")
                {
                    return false;
                }
            }

            return true;
        }

        public static string lettreV(string[][] array)
        {
            string sortie = "A";
            sortie = lettre0(array);
            if (sortie == "A")
            {
                sortie = lettre180(array);
            }
            return sortie;
        }


        public static string lettreH(string[][] array)
        {
            string sortie = "A";
            sortie = lettre90(array);
            if (sortie == "A")
            {
                sortie = lettre270(array);
            }
            return sortie;
        }

        protected bool IsValidLetter(String letter)
        {
            return letter == "C" || letter == "O" || letter == "V" || letter == "E";
        }

        /* retourne la matrice [5][3] si verticale et [3][5] si horzontale
         x et y represente lindice du coin haut gauche dans la matrice totale*/
        public string[][] ExtractSubMatrix(int x, int y, bool horizontale, bool hasPadding = false)
        {
            int smallerDimension = 3;
            int biggerDimension = 5;
            if (hasPadding)
            {
                smallerDimension += 2;
                biggerDimension += 2;
            }

            string[][] sortie;
            if (horizontale)
            {
                sortie = new string[smallerDimension][];
                try
                {
                    for (int j = y; j < y + smallerDimension; j++)
                    {
                        sortie[j - y] = new string[biggerDimension];
                        for (int i = x; i < x + biggerDimension; i++)
                        {
                            sortie[j - y][i - x] = Grid[j][i];

                        }
                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    for (int i = 0; i < sortie.Count(); i++)
                    {
                        sortie[i] = new string[biggerDimension];
                        for (int j = 0; j < sortie[0].Count(); j++)
                        {
                            sortie[i][j] = "X";
                        }
                    }
                }
            }
            else
            {
                sortie = new string[biggerDimension][];
                try
                {
                    for (int j = y; j < y + biggerDimension; j++)
                    {
                        sortie[j - y] = new string[smallerDimension];
                        for (int i = x; i < x + smallerDimension; i++)
                        {
                            sortie[j - y][i - x] = Grid[j][i];

                        }
                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    for (int i = 0; i < sortie.Count(); i++)
                    {
                        sortie[i] = new string[smallerDimension];
                        for (int j = 0; j < sortie[0].Count(); j++)
                        {
                            sortie[i][j] = "X";
                        }
                    }
                }
            }
            return sortie;
        }
        public abstract string GetSolution();
        /*
        public override string Tostring()
        {
            var lines = new List<string>();
            foreach (List<string> line in Grid)
            {
                lines.Add(string.Join("", line));
            }
            return string.Join("\n", lines);
        }*/


        public String[][] GetPuzzleRow(int startIndex, int rowHeight = 6)
        {
            String[][] outRows = new string[rowHeight][];
            for (int i = 0; i < rowHeight; i++)
            {
                outRows[i] = Grid[startIndex * rowHeight + i];
            }
            return outRows;
        }
        public int TrouverPremiereColonne(String[][] array)
        {
            bool trouver = false;
            int sortie = -1;
            while (!trouver)
            {
                sortie += 1;
                for (int i = 0; i < 6; i++)
                {
                    if (array[i][sortie] == "X")
                    {
                        trouver = true;
                        break;
                    }

                }
            }

            return sortie;
        }
    }
}
