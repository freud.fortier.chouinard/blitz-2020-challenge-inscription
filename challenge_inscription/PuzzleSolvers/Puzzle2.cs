﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle2 : AbstractPuzzle
    {
        public Puzzle2(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            var solution = "";
            if (Grid.Count() == 5)
            {
                solution = lettre0(Grid);
                if (!IsValidLetter(solution))
                    solution = lettre180(Grid);
            }
            else
            {
                solution = lettre90(Grid);
                if (!IsValidLetter(solution))
                    solution = lettre270(Grid);
            }
            return solution;
        }
    }
}
