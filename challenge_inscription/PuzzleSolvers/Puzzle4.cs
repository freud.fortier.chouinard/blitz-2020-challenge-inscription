﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle4 : AbstractPuzzle
    {
        public Puzzle4(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            String CurrentLetter = "A";
            string Solution = "";

            for (int y = 0; y < 3; y++)
            {
                for (int x = 1; x < Grid[0].Count(); )
                {
                    CurrentLetter = lettreV(ExtractSubMatrix(x, y, false));
                    if (!IsValidLetter(CurrentLetter))
                    {

                        CurrentLetter = lettreH(ExtractSubMatrix(x, y, true));
                        if (IsValidLetter(CurrentLetter))
                            x += 6;
                        else
                            x++;
                    }
                    else
                    {
                        x += 4;
                    }
                    if(IsValidLetter(CurrentLetter))
                    Solution += CurrentLetter;
                }
            }
            return Solution;
        }
    }
}