﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle5 : AbstractPuzzle
    {
        public Puzzle5(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            string Solution = lettre0(ExtractSubMatrix(1, 1, false)) + lettre0(ExtractSubMatrix(5, 1, false)) + lettre0(ExtractSubMatrix(9, 1, false)) + lettre0(ExtractSubMatrix(13, 1, false)) + lettre0(ExtractSubMatrix(17, 1, false));
            Solution += lettre0(ExtractSubMatrix(1, 7, false)) + lettre0(ExtractSubMatrix(5, 7, false)) + lettre0(ExtractSubMatrix(9, 7, false)) + lettre0(ExtractSubMatrix(13, 7, false)) + lettre0(ExtractSubMatrix(17, 7, false));
            Solution += lettre0(ExtractSubMatrix(1, 13, false)) + lettre0(ExtractSubMatrix(5, 13, false)) + lettre0(ExtractSubMatrix(9, 13, false)) + lettre0(ExtractSubMatrix(13, 13, false)) + lettre0(ExtractSubMatrix(17, 13, false));
            Solution += lettre0(ExtractSubMatrix(1, 19, false)) + lettre0(ExtractSubMatrix(5, 19, false)) + lettre0(ExtractSubMatrix(9, 19, false)) + lettre0(ExtractSubMatrix(13, 19, false)) + lettre0(ExtractSubMatrix(17, 19, false));
            Solution += lettre0(ExtractSubMatrix(1, 25, false)) + lettre0(ExtractSubMatrix(5, 25, false)) + lettre0(ExtractSubMatrix(9, 25, false)) + lettre0(ExtractSubMatrix(13, 25, false)) + lettre0(ExtractSubMatrix(17, 25, false));

            return Solution;
        }
    }
}
