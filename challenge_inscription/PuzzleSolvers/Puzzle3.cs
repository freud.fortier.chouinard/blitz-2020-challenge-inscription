﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace challenge_inscription.PuzzleSolvers
{
    public class Puzzle3 : AbstractPuzzle
    {
        public Puzzle3(string[][] grid) : base(grid) { }

        public override string GetSolution()
        {
            return lettre0(ExtractSubMatrix(1, 0, false)) 
                + lettre0(ExtractSubMatrix(5, 0, false)) 
                + lettre0(ExtractSubMatrix(9, 0, false)) 
                + lettre0(ExtractSubMatrix(13, 0, false)) 
                + lettre0(ExtractSubMatrix(17, 0, false));

        }
    }
}
