﻿using System;
using System.Collections.Generic;
using System.Linq;
using challenge_inscription.PuzzleSolvers;

namespace challenge_inscription
{
    public class ChallengeResponse
    {
        public string TeamName { get; set; }

        public string TeamStreetAddress { get; set; }

        public List<Participant> Participants { get; set; }

        public string[] Solutions { get; set; }

        public void SolvePuzzles(string[][][] PackedPuzzles)
        {
            var PuzzleSolvers = new AbstractPuzzle[] { 
                new Puzzle1(PackedPuzzles[0]),
                new Puzzle2(PackedPuzzles[1]),
                new Puzzle3(PackedPuzzles[2]),
                new Puzzle4(PackedPuzzles[3]),
                new Puzzle5(PackedPuzzles[4]),
                new Puzzle6(PackedPuzzles[5]),
                new Puzzle7(PackedPuzzles[6]),
                new Puzzle8(PackedPuzzles[7]),
            };

            // We only add the solutions to puzzles we are able to solve.
            Solutions = new string[] {
               // PuzzleSolvers[0].GetSolution(),
               // PuzzleSolvers[1].GetSolution(),
              //  PuzzleSolvers[2].GetSolution(),
              //  PuzzleSolvers[3].GetSolution(),
             //   PuzzleSolvers[4].GetSolution(),
              //  PuzzleSolvers[5].GetSolution(),
              //  PuzzleSolvers[6].GetSolution(),
                PuzzleSolvers[7].GetSolution(),
            };
        }
    }
}
