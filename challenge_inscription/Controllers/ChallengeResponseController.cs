﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Newtonsoft.Json;

namespace challenge_inscription.Controllers
{
    [ApiController]
    [Route("/")]
    public class ChallengeResponseController : ControllerBase
    {
        private static readonly List<Participant> Participants = new List<Participant>
        {
            new Participant
            {
                IsCaptain = false,
                FullName = "Frédéric Fortier-Chouinard",
                Email = "freud.fortier.chouinard@gmail.com",
                GoogleAccount = "freud.fortier.chouinard@gmail.com",
                Phone = "4186596355",
                School = "Université Laval",
                SchoolProgram = "Génie informatique",
                GraduationDate = 1682261596000,
                ShirtSize = "m-M"
            },

            new Participant
            {
                IsCaptain = true,
                FullName = "Louis-David Deschenes",
                Email = "ldesch1998@hotmail.com",
                GoogleAccount = "ldesch1998@gmail.com",
                Phone = "4189302417",
                School = "McGill",
                SchoolProgram = "Computer Science",
                GraduationDate = 1648785600000,
                ShirtSize = "m-XXL"
            },

            new Participant
            {
                IsCaptain = false,
                FullName = "Antony Martel",
                Email = "antonyma2222@gmail.com",
                GoogleAccount = "antonyma2222@gmail.com",
                Phone = "5817771302",
                School = "Université Laval",
                SchoolProgram = "Actuariat",
                GraduationDate = 1619841600000,
                ShirtSize = "m-XXL"
            },

            new Participant
            {
                IsCaptain = false,
                FullName = "Victor Frève-Boucher",
                Email = "vicfreve@gmail.com",
                GoogleAccount = "vicfreve@gmail.com",
                Phone = "5817489889",
                School = "Université de Sherbrooke",
                SchoolProgram = "Génie informatique",
                GraduationDate = 1701406800000,
                ShirtSize = "m-M"
            },

        };

        private static readonly ChallengeResponse FlatResponse = new ChallengeResponse
        {
            TeamName = "The Flat Earthers",
            TeamStreetAddress = "1464, rue Louis-Francoeur",
            Participants = Participants

        };

        private readonly ILogger<ChallengeResponseController> _logger;

        public ChallengeResponseController(ILogger<ChallengeResponseController> logger)
        {
            _logger = logger;
        }

        private void logPuzzles(string PackedPuzzles)
        {

            var logPath = System.IO.Path.GetTempFileName();
            var logFile = System.IO.File.Create(logPath);
            var logWriter = new System.IO.StreamWriter(logFile);
            logWriter.WriteLine(PackedPuzzles);
            logWriter.Dispose();
        }

        [HttpPost]
        public ChallengeResponse PostPuzzle(string[][][] Puzzles)
        {

            logPuzzles(JsonConvert.SerializeObject(Puzzles));
            FlatResponse.SolvePuzzles(Puzzles);
            return FlatResponse;
        }
    }
}
